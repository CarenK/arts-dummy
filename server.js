const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const app = express();
const {PORT, CLIENT_ORIGIN, DATABASE_URL} = require('./config');

app.use(express.static('public'));
app.use(morgan('common'))
app.use(express.json());
app.use(
  cors({
    origin: CLIENT_ORIGIN
  })
)

app.get('/api/*', (req, res) => {
    res.json({ok: true});
});

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));

module.exports = {app};